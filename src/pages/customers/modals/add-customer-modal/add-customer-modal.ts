import { Component, OnInit } from "@angular/core";
import { NavController, NavParams, AlertController, ViewController, ToastController, IonicPage, ActionSheetController, LoadingController, Platform } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ApiServiceProvider } from "../../../../providers/api-service/api-service";
import * as moment from 'moment';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
declare var cordova: any;
@IonicPage()
@Component({
    selector: 'page-add-customer-model',
    templateUrl: './add-customer-modal.html'
})
export class AddCustomerModal implements OnInit {

    addcustomerform: FormGroup;
    customerdata: any = {};
    Customeradd: any;
    submitAttempt: boolean;
    selectDealer: any;
    isSuperAdminStatus: boolean;
    islogin: any;
    dealerdata: any;
    currentYear: any;
    // isDealer: any;
    DlType = [{
        value: 'dl',
        viewValue: "Driving License"
    }, {
        value: 'Adhar',
        viewValue: "Adhar Card"
    }, {
        value: 'PAN',
        viewValue: "PAN Card"
    }, {
        value: 'voterCard',
        viewValue: "Voter ID Card"
    }

    ];
    Documentdata: any;
    Documentdatashow: any;
    DocumentdataAdhar: any;
    lastImage: string = null;
    Imgloading: any;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        public apicallCustomer: ApiServiceProvider,
        public alerCtrl: AlertController,
        public viewCtrl: ViewController,
        public toastCtrl: ToastController,
        public actionSheetCtrl: ActionSheetController,
        public file: File,
        public filePath: FilePath,
        public camera: Camera,
        public transferObj: TransferObject,
        public transfer: Transfer,
        public loadingCtrl: LoadingController,
        public platform: Platform
    ) {

        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        this.isSuperAdminStatus = this.islogin.isSuperAdmin
        console.log("isDealer=> " + this.isSuperAdminStatus);

        var tempdate = new Date();
        tempdate.setDate(tempdate.getDate() + 365);

        this.currentYear = moment(new Date(tempdate), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log(this.currentYear);

        this.addcustomerform = formBuilder.group({
            userId: ['', Validators.required],
            Firstname: ['', Validators.required],
            LastName: ['', Validators.required],
            emailid: [this.islogin.account, Validators.required],
            contact_num: [''],
            password: ['', Validators.required],
            confpassword: [''],
            address: ['', Validators.required],
            ExipreDate: [this.currentYear, Validators.required],
            dealer_firstname: [''],
            DlNo: [""],
            Name: [""]
        })
    }

    ngOnInit() {
        this.getAllDealers();
    }

    DocumentOnChnage(type) {
        console.log(type);
        this.Documentdata = type;
        console.log("type id=> " + this.Documentdata.value);
        if (this.Documentdata.value == 'dl') {
            this.Documentdatashow = this.Documentdata.value;
            console.log(this.Documentdatashow);
        } else if (this.Documentdata.value == 'Adhar') {
            this.DocumentdataAdhar = this.Documentdata.value;
            console.log(this.DocumentdataAdhar);
        }

    }

    // Create a new name for the image
    private createFileName() {
        var d = new Date(),
            n = d.getTime(),
            newFileName = n + ".jpg";
        return newFileName;
    }

    // Copy the image to a local folder
    private copyFileToLocalDir(namePath, currentName, newFileName) {
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
            this.lastImage = newFileName;
        }, error => {
            this.presentToast('Error while storing file.');
        });
    }

    private presentToast(text) {
        let toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    }

    // Always get the accurate path to your apps folder
    public pathForImage(img) {
        if (img === null) {
            return '';
        } else {
            return cordova.file.dataDirectory + img;
        }
    }


    public uploadImage() {
        // Destination URL
        var url = "https://www.oneqlik.in/users/uploadImage";
        console.log('1');
        // File for Upload
        var targetPath = this.pathForImage(this.lastImage);
        console.log("TargetPath=>", targetPath);

        // File name only
        var filename = this.lastImage;
        console.log(filename);
        var options = {
            fileKey: "photo",
            fileName: filename,
            chunkedMode: false,
            mimeType: "image/jpeg",
            params: { 'fileName': filename }
        };
        // multipart/form-data"
        this.transferObj = this.transfer.create();

        this.Imgloading = this.loadingCtrl.create({
            content: 'Uploading...',
        });
        this.Imgloading.present();
        console.log('12');

        // this.apiCall.uploadImage(options).subscribe(res=>{
        //   console.log(res);
        // })
        // Use the FileTransfer to upload the image
        this.transferObj.upload(targetPath, url, options).then(data => {
            console.log("responseUrl=>", data.response);

            // var image = data.response;
            // var splitImage = image.split("/");s
            // var img = splitImage[1];
            // var removeQuote = img.split('"');
            // this.imgString = "/" + removeQuote[0];
            // console.log(this.imgString);
            this.Imgloading.dismissAll();
            //this.dlUpdate(data.response);


        }, err => {
            console.log("uploadError=>", err)
            this.lastImage = null;
            this.Imgloading.dismissAll();
            this.presentToast('Error while uploading file, Please try again !!!');
        });
    }

    public takePicture(sourceType) {
        // Create options for the Camera Dialog
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };

        // Get the data of an image
        this.camera.getPicture(options).then((imagePath) => {
            // Special handling for Android library
            if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
                this.filePath.resolveNativePath(imagePath)
                    .then(filePath => {
                        let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                        let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
                    });
            } else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
            }
        }, (err) => {
            this.presentToast('Error while selecting image.');
        });
    }

    public presentActionSheet() {
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    handler: () => {
                        this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: () => {
                        this.takePicture(this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    }

    dealerOnChnage(dealer) {
        console.log(dealer);
        this.dealerdata = dealer;
        console.log("dealer id=> " + this.dealerdata.dealer_id);
    }

    addcustomer() {
        this.submitAttempt = true;
        // console.log(devicedetails);
        if (this.addcustomerform.valid) {
            debugger;
            if (this.islogin.isSuperAdmin == true) {
                this.customerdata = {
                    "first_name": this.addcustomerform.value.Firstname,
                    "last_name": this.addcustomerform.value.LastName,
                    "email": this.addcustomerform.value.emailid,
                    "phone": this.addcustomerform.value.contact_num,
                    "password": this.addcustomerform.value.password,
                    "isDealer": false,
                    "custumer": true,
                    "status": true,
                    "user_id": this.addcustomerform.value.userId,
                    "address": this.addcustomerform.value.address,
                    "supAdmin": this.islogin._id
                }
            } else {
                if (this.islogin.isDealer == true) {
                    this.customerdata = {
                        "first_name": this.addcustomerform.value.Firstname,
                        "last_name": this.addcustomerform.value.LastName,
                        "email": this.addcustomerform.value.emailid,
                        "phone": this.addcustomerform.value.contact_num,
                        "password": this.addcustomerform.value.password,
                        "isDealer": this.islogin.isDealer,
                        "custumer": true,
                        "status": true,
                        "user_id": this.addcustomerform.value.userId,
                        "address": this.addcustomerform.value.address,
                        "supAdmin": this.islogin.supAdmin,
                        // "Dealer": this.islogin.Dealer_ID['_id']
                    }
                }
            }

            if (this.dealerdata != undefined) {
                this.customerdata.Dealer = this.dealerdata.dealer_id;
            } else {
                this.customerdata.Dealer = this.islogin._id;
            }

            this.apicallCustomer.startLoading().present();
            this.apicallCustomer.signupApi(this.customerdata)
                .subscribe(data => {
                    this.apicallCustomer.stopLoading();
                    this.Customeradd = data;

                    console.log("devicesadd=> ", this.Customeradd)
                    let toast = this.toastCtrl.create({
                        message: 'Customer was added successfully',
                        position: 'top',
                        duration: 1500
                    });

                    toast.onDidDismiss(() => {
                        console.log('Dismissed toast');
                        this.viewCtrl.dismiss();
                    });

                    toast.present();
                },
                    err => {
                        this.apicallCustomer.stopLoading();
                        var body = err._body;
                        console.log(body)
                        var msg = JSON.parse(body);
                        console.log(msg)
                        var namepass = [];
                        namepass = msg.split(":");
                        var name = namepass[1];

                        let alert = this.alerCtrl.create({
                            title: 'Oops!',
                            message: name,
                            buttons: ['OK']
                        });
                        alert.present();
                        console.log(err);
                    });
        }
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    getAllDealers() {
        console.log("get dealer");

        var baseURLp = 'https://www.oneqlik.in/users/getAllDealerVehicles';

        this.apicallCustomer.getAllDealerCall(baseURLp)
            .subscribe(data => {
                this.selectDealer = data;
                console.log(this.selectDealer);
            },
                error => {
                    console.log(error)
                });
    }


}



