import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, Navbar, Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
// import { DatePicker } from '@ionic-native/date-picker';
import * as moment from 'moment';
import { GoogleMaps, Marker, LatLng, Spherical, GoogleMapsEvent, HtmlInfoWindow, LatLngBounds, Geocoder, GeocoderResult, GoogleMapsMapTypeId } from '@ionic-native/google-maps';
import { DrawerState } from 'ion-bottom-drawer';

@IonicPage()
@Component({
  selector: 'page-history-device',
  templateUrl: 'history-device.html',
})
export class HistoryDevicePage implements OnInit, OnDestroy {
  @ViewChild(Navbar) navBar: Navbar;

  shouldBounce = true;
  dockedHeight = 100;
  distanceTop = 378;
  drawerState = DrawerState.Docked;
  states = DrawerState;
  minimumHeight = 0;
  // drawerHidden = true;
  // shouldBounce = true;
  // dockedHeight = 150;
  // bounceThreshold = 500;
  // distanceTop = 56;
  showActionSheet: boolean = false;

  device: any;
  trackerId: any;
  trackerType: any;
  DeviceId: any;
  datetimeStart: any;
  datetimeEnd: any;
  hideplayback: boolean;
  trackerName: any;
  avg_speed: string;
  total_dis: string;
  data2: any;
  latlongObjArr: any;
  locations: any = [];
  islogin: any;
  dataArrayCoords: any[];
  mapData: any[];
  speed: number;
  flightPath: any;
  arrival: Date;
  departure: Date;
  target: number;
  playing: boolean;
  coordreplaydata: any;
  speedMarker: any;
  updatetimedate: any;
  showDropDown: boolean;
  SelectVehicle: string = 'Select Vehicle';
  devices1243: any[];
  devices: any;
  isdevice: any;
  portstemp: any;
  selectedVehicle: any;
  totime: string;
  fromtime: string;
  allData: any = {};
  startPos: any[];
  showZoom: boolean = false;
  address: any;
  arrTime: any;
  depTime: any;
  addressofstudent: any;
  drawerHidden1: boolean;
  arrivalTime: string;
  departureTime: string;
  addressof: string;
  durations: string;
  menuActive: boolean;

  constructor(
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public apiCall: ApiServiceProvider,
    // private datePicker: DatePicker
  ) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};

    this.datetimeStart = moment({ hours: 0 }).format();
    console.log('start date', this.datetimeStart)
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    console.log('stop date', this.datetimeEnd);
  }

  setDocHeight() {
    console.log("dockerchage event")
    this.dockedHeight = 150;
    this.distanceTop = 46;
  }

  closeDocker() {
    let that = this;
    that.showActionSheet = false;
  }
  ngOnInit() {
    if (localStorage.getItem("SCREEN") != null) {
      this.navBar.backButtonClick = (e: UIEvent) => {
        // todo something
        // this.navController.pop();
        console.log("back button poped")
        if (localStorage.getItem("SCREEN") != null) {
          if (localStorage.getItem("SCREEN") === 'live') {
            this.navCtrl.setRoot('LivePage');
          } else {
            if (localStorage.getItem("SCREEN") === 'dashboard') {
              this.navCtrl.setRoot('DashboardPage')
            }
          }
        }
      }
    }
    // else {
    //   this.navCtrl.pop();
    // }

    localStorage.removeItem("markerTarget");
    localStorage.removeItem("speedMarker");
    localStorage.removeItem("updatetimedate");

    if (localStorage.getItem("MainHistory") != null) {
      console.log("coming soon")
      this.showDropDown = true;
      this.getdevices();
    } else {
      this.device = this.navParams.get('device');
      console.log("devices=> ", this.device);
      this.trackerId = this.device.Device_ID;
      this.trackerType = this.device.iconType;
      this.DeviceId = this.device._id;
      this.trackerName = this.device.Device_Name;
      this.btnClicked(this.datetimeStart, this.datetimeEnd)
    }
    this.hideplayback = false;
    this.target = 0;
  }


  ngOnDestroy() {
    localStorage.removeItem("markerTarget");
    localStorage.removeItem("speedMarker");
    localStorage.removeItem("updatetimedate");
    localStorage.removeItem("MainHistory");
  }

  changeformat(date) {
    console.log("date=> " + new Date(date).toISOString())
  }

  getdevices() {
    console.log("getdevices");
    var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    // var baseURLp = 'http://13.126.36.205:3000/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
    this.apiCall.startLoading().present();
    // this.apiCall.getdevicesApi(this.islogin._id, this.islogin.email)
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.portstemp = data.devices;
        this.devices1243 = [];
        this.devices = data;
        this.devices1243.push(data);
        localStorage.setItem('devices', this.devices);
        this.isdevice = localStorage.getItem('devices');
        for (var i = 0; i < this.devices1243[i]; i++) {
          this.devices1243[i] = {
            'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
          };
        }
      },
        error => {
          this.apiCall.stopLoading();
          console.log(error);
        });
  }

  onChangedSelect(item) {
    debugger;
    let that = this;
    that.trackerId = item.Device_ID;
    that.trackerType = item.iconType;
    that.DeviceId = item._id;
    that.trackerName = item.Device_Name;
    if (that.allData.map) {
      that.allData.map.clear();
      that.allData.map.remove();
    }
  }

  Playback() {
    let that = this;
    that.showZoom = true;
    if (localStorage.getItem("markerTarget") != null) {
      that.target = JSON.parse(localStorage.getItem("markerTarget"));
    }
    that.playing = !that.playing; // This would alternate the state each time

    var coord = that.dataArrayCoords[that.target];
    that.coordreplaydata = coord;
    var lat = coord[0];
    var lng = coord[1];

    that.startPos = [lat, lng];
    that.speed = 200; // km/h

    if (that.playing) {
      that.allData.map.setCameraTarget({ lat: lat, lng: lng });
      if (that.allData.mark == undefined) {

        that.allData.map.addMarker({
          icon: './assets/imgs/vehicles/running' + that.trackerType + '.png',
          styles: {
            'text-align': 'center',
            'font-style': 'italic',
            'font-weight': 'bold',
            'color': 'green'
          },
          position: new LatLng(that.startPos[0], that.startPos[1]),
        }).then((marker: Marker) => {
          that.allData.mark = marker;
          that.liveTrack(that.allData.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
        });
      } else {
        that.allData.mark.setPosition(new LatLng(that.startPos[0], that.startPos[1]));
        that.liveTrack(that.allData.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
      }
    } else {
      that.allData.mark.setPosition(new LatLng(that.startPos[0], that.startPos[1]));
    }
  }

  liveTrack(map, mark, coords, target, startPos, speed, delay) {
    let that = this;
    that.events.subscribe("SpeedValue:Updated", (sdata) => {
      speed = sdata;
    })
    var target = target;
    if (!startPos.length)
      coords.push([startPos[0], startPos[1]]);

    function _gotoPoint() {
      if (target > coords.length)
        return;

      var lat = mark.getPosition().lat;
      var lng = mark.getPosition().lng;

      var step = (speed * 1000 * delay) / 3600000;
      if (coords[target] == undefined)
        return;
      var dest = new LatLng(coords[target][0], coords[target][1]);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); //in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target][0] - lat) / numStep;
      var deltaLng = (coords[target][1] - lng) / numStep;

      function changeMarker(mark, deg) {
        mark.setRotation(deg);
      }

      function _moveMarker() {
        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        if (i < distance) {
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }

          mark.setPosition(new LatLng(lat, lng));
          map.setCameraTarget(new LatLng(lat, lng))
          setTimeout(_moveMarker, delay);
        } else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }

          mark.setPosition(dest);
          map.setCameraTarget(dest);
          target++;
          setTimeout(_gotoPoint, delay);
        }
      }
      a++
      if (a > coords.length) {

      } else {
        that.speedMarker = coords[target][3].speed;
        that.updatetimedate = coords[target][2].time;

        if (that.playing) {
          _moveMarker();
          target = target;
          localStorage.setItem("markerTarget", target);

        } else { }
        // km_h = km_h;
      }
    }
    var a = 0;
    _gotoPoint();
  }

  zoomin() {
    let that = this;
    that.allData.map.moveCameraZoomIn();
  }
  zoomout() {
    let that = this;
    that.allData.map.animateCameraZoomOut();
  }

  inter(fastforwad) {
    // debugger
    let that = this;
    console.log("fastforwad=> " + fastforwad);
    if (fastforwad == 'fast') {
      that.speed = 2 * that.speed;
      console.log("speed fast=> " + that.speed)
    }
    else if (fastforwad == 'slow') {
      if (that.speed > 50) {
        that.speed = that.speed / 2;
        console.log("speed slow=> " + that.speed)
      }
      else {
        console.log("speed normal=> " + that.speed)
      }
    }
    else {
      that.speed = 200;
    }
    that.events.publish("SpeedValue:Updated", that.speed)
  }

  btnClicked(timeStart, timeEnd) {
    if (localStorage.getItem("MainHistory") != null) {
      if (this.selectedVehicle == undefined) {
        let alert = this.alertCtrl.create({
          message: "Please select the vehicle first!!",
          buttons: ['OK']
        });
        alert.present();
      } else {
        this.maphistory(timeStart, timeEnd);
      }
    } else {
      this.maphistory(timeStart, timeEnd);
    }
  }

  maphistory(timeStart, timeEnd) {

    var from1 = new Date(timeStart);
    this.fromtime = from1.toISOString();
    var to1 = new Date(timeEnd);
    this.totime = to1.toISOString();

    if (this.totime >= this.fromtime) {

    } else {
      let alert = this.alertCtrl.create({
        title: 'Select Correct Time',
        message: 'To time always greater than From Time',
        buttons: ['ok']
      });
      alert.present();
      return false;
    }

    this.apiCall.startLoading().present();
    this.apiCall.getDistanceSpeedCall(this.trackerId, this.fromtime, this.totime)
      .subscribe(data3 => {
        this.data2 = data3;
        this.latlongObjArr = data3;

        if (this.data2["Average Speed"] == 'NaN') {
          this.data2.AverageSpeed = 0;
        } else {
          this.data2.AverageSpeed = this.data2["Average Speed"];
        }

        this.data2.IdleTime = this.data2["Idle Time"];
        this.hideplayback = true;
        // this.customTxt = "<html> <head><style> </style> </head><body>Total Distance - " + this.total_dis + " Km<br>Average Speed - " + this.avg_speed + " Km/hr</body> </html> "

        //////////////////////////////////
        this.locations = [];
        this.stoppages(timeStart, timeEnd);
        ////////////////////////////////
      },
        error => {
          this.apiCall.stopLoading();
          console.log("error in getdistancespeed =>  ", error)
          var body = error._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            message: msg.message,
            buttons: ['okay']
          });
          alert.present();
        });

  }

  stoppages(timeStart, timeEnd) {
    let that = this;
    that.apiCall.stoppage_detail(this.islogin._id, new Date(timeStart).toISOString(), new Date(timeEnd).toISOString(), this.DeviceId)
      .subscribe(res => {
        console.log('stoppage data', res)
        var arr = [];
        for (var i = 0; i < res.length; i++) {

          this.arrivalTime = new Date(res[i].arrival_time).toLocaleString();
          this.departureTime = new Date(res[i].departure_time).toLocaleString();

          var fd = new Date(this.arrivalTime).getTime();
          var td = new Date(this.departureTime).getTime();
          var time_difference = td - fd;
          var total_min = time_difference / 60000;
          var hours = total_min / 60
          var rhours = Math.floor(hours);
          var minutes = (hours - rhours) * 60;
          var rminutes = Math.round(minutes);
          var Durations = rhours + 'Hours' + ':' + rminutes + 'Min';

          arr.push({
            lat: res[i].lat,
            lng: res[i].long,
            arrival_time: res[i].arrival_time,
            departure_time: res[i].departure_time,
            device: res[i].device,
            address: res[i].address,
            user: res[i].user,
            duration: Durations
          });

          that.locations.push(arr)

        }
        console.log('stoppage data locations', that.locations)
        that.callgpsFunc(that.fromtime, that.totime);

      },
        err => {
          this.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            message: msg.message,
            buttons: ['okay']
          });
          alert.present();
        });
  }

  callgpsFunc(fromtime, totime) {
    let that = this;
    that.apiCall.gpsCall(this.trackerId, fromtime, totime)
      .subscribe(data3 => {
        that.apiCall.stopLoading();
        if (data3.length > 0) {
          if (data3.length > 1) {   // to draw polyline at least need two points
            that.gps(data3.reverse());
          } else {
            let alert = that.alertCtrl.create({
              message: 'No Data found for selected vehicle..',
              buttons: [{
                text: 'OK',
                handler: () => {
                  // that.datetimeStart = moment({ hours: 0 }).format();
                  // console.log('start date', this.datetimeStart)
                  // that.datetimeEnd = moment().format();//new Date(a).toISOString();
                  // console.log('stop date', this.datetimeEnd);

                  // that.selectedVehicle = undefined;
                  that.hideplayback = false;
                }
              }]
            });
            alert.present();
          }
        } else {
          let alert = that.alertCtrl.create({
            message: 'No Data found for selected vehicle..',
            buttons: [{
              text: 'OK',
              handler: () => {
                // that.datetimeStart = moment({ hours: 0 }).format();
                // console.log('start date', this.datetimeStart)
                // that.datetimeEnd = moment().format();//new Date(a).toISOString();
                // console.log('stop date', this.datetimeEnd);
                // that.selectedVehicle = undefined;
                that.hideplayback = false;
              }
            }]
          });
          alert.present();
        }

      },
        err => {
          that.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = that.alertCtrl.create({
            message: msg.message,
            buttons: ['okay']
          });
          alert.present();
        });
  }

  gps(data3) {

    let that = this;
    that.latlongObjArr = data3;
    that.dataArrayCoords = [];
    for (var i = 0; i < data3.length; i++) {
      if (data3[i].lat && data3[i].lng) {
        var arr = [];
        var startdatetime = new Date(data3[i].insertionTime);
        arr.push(data3[i].lat);
        arr.push(data3[i].lng);
        arr.push({ "time": startdatetime.toLocaleString() });
        arr.push({ "speed": data3[i].speed });
        that.dataArrayCoords.push(arr);
      }
    }

    that.mapData = [];
    that.mapData = data3.map(function (d) {
      return { lat: d.lat, lng: d.lng };
    })
    that.mapData.reverse();

    if (that.allData.map != undefined) {
      that.allData.map.remove();
    }

    let bounds = new LatLngBounds(that.mapData);

    let mapOptions = {
      gestures: {
        rotate: false,
        tilt: false
      }
    }

    that.allData.map = GoogleMaps.create('map_canvas', mapOptions);
    that.allData.map.moveCamera({
      target: bounds
    })
    this.allData.map.on(GoogleMapsEvent.MAP_CLICK).subscribe(
      (data) => {
        console.log('Click MAP');

        that.drawerHidden1 = true;
      }
    );
    if (that.locations[0] != undefined) {              // check if there is stoppages or not
      for (var k = 0; k < that.locations[0].length; k++) {
        that.setStoppages(that.locations[0][k]);
      }
    }

    that.allData.map.addMarker({
      title: 'S',
      position: that.mapData[0],
      icon: 'red',
      styles: {
        'text-align': 'center',
        'font-style': 'italic',
        'font-weight': 'bold',
        'color': 'red'
      },
    }).then((marker: Marker) => {
      marker.showInfoWindow();

      that.allData.map.addMarker({
        title: 'D',
        position: that.mapData[that.mapData.length - 1],
        icon: 'green',
        styles: {
          'text-align': 'center',
          'font-style': 'italic',
          'font-weight': 'bold',
          'color': 'green'
        },
      }).then((marker: Marker) => {
        marker.showInfoWindow();
      });
    });

    that.allData.map.addPolyline({
      points: that.mapData,
      color: '#635400',
      width: 3,
      geodesic: true
    })
  }

  setStoppages(pdata) {
    let that = this;

    ///////////////////////////////
    let htmlInfoWindow = new HtmlInfoWindow();
    let frame: HTMLElement = document.createElement('div');
    frame.innerHTML = [
      '<p style="font-size: 7px;">Address:- ' + pdata.address + '</p>',
      '<p style="font-size: 7px;">Arrival Time:- ' + moment(new Date(pdata.arrival_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a") + '</p>',
      '<p style="font-size: 7px;">Departure Time:- ' + moment(new Date(pdata.departure_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a") + '</p>'
    ].join("");
    // frame.getElementsByTagName("img")[0].addEventListener("click", () => {
    //   htmlInfoWindow.setBackgroundColor('red');
    // });
    htmlInfoWindow.setContent(frame, { width: "220px", height: "100px" });
    ///////////////////////////////////////////////////

    if (pdata != undefined)
      (function (data) {
        console.log("inside for data=> ", data)

        var centerMarker = data;
        let location = new LatLng(centerMarker.lat, centerMarker.lng);
        let markerOptions = {
          position: location,
          icon: './assets/imgs/park.png'
        };
        that.allData.map.addMarker(markerOptions)
          .then((marker: Marker) => {
            console.log('centerMarker.ID' + centerMarker.ID)
            marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
              .subscribe(e => {
                console.log(e);
                console.log(e.lat);
                that.showActionSheet = true;
                that.drawerState = DrawerState.Docked;
                // that.drawerHidden1 = false;
                console.log("latt1", e[0].lat);
                console.log("long1", e[0].lng);
                Geocoder.geocode({
                  "position": {
                    lat: e[0].lat,
                    lng: e[0].lng
                  }
                }).then((results: GeocoderResult[]) => {
                  if (results.length == 0) {
                    //not found
                    return null;
                  }
                  // console.log('address=>',results[0]);
                  // let address: any = [   
                  // results[0].subThoroughfare || "",
                  // results[0].thoroughfare || "",
                  // results[0].locality || "",
                  // results[0].adminArea || "",
                  // results[0].postalCode || "",
                  // results[0].country || ""
                  // ].join(", ");
                  that.addressof = results[0].extra.lines[0];
                  console.log("pickup location ", that.addressof);
                  // that.sendAddress(that.addressofstudent,studentid,that.markerlatlong.lat,that.markerlatlong.lng);
                });
                // htmlInfoWindow.open(marker);

                setTimeout(function () {
                  console.log('time out call')
                  that.address = that.addressof;
                  console.log("pickup location new ", that.address);
                  that.durations = data.duration;
                  that.arrTime = moment(new Date(data.arrival_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a");
                  that.depTime = moment(new Date(data.departure_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a");
                }, 500);

              });
          });

      })(pdata)
  }

  onClickMainMenu(item) {
    this.menuActive = !this.menuActive;
  }
  onClickMap(maptype) {
    let that = this;
    if (maptype == 'SATELLITE') {
      that.allData.map.setMapTypeId(GoogleMapsMapTypeId.SATELLITE);
    } else {
      if (maptype == 'TERRAIN') {
        that.allData.map.setMapTypeId(GoogleMapsMapTypeId.TERRAIN);
      } else {
        if (maptype == 'NORMAL') {
          that.allData.map.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
        }
      }
    }
  }
}
