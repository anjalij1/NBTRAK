import { Component } from '@angular/core';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import { NavParams, ViewController, IonicPage } from 'ionic-angular';
import { GeocoderProvider } from '../../../providers/geocoder/geocoder';

@IonicPage()
@Component({
  selector: 'fuel-events',
  templateUrl: 'fuel-events.html'
})
export class FuelEventsComponent {
  fuelData: any[] = [];
  eventType: string;
  constructor(
    private apiservice: ApiServiceProvider,
    private navParams: NavParams,
    private viewCtrl: ViewController,
    private geocoderApi: GeocoderProvider,
  ) {
    console.log("param maps: ", this.navParams.get("paramMaps"));
    // if (!this.navParams.get('paramMaps') || !this.navParams.get('event')) {
      if (!this.navParams.get('paramMaps')) {
      this.viewCtrl.dismiss();
      return;
    }
    this.fuelData = this.navParams.get("paramMaps");
    this.eventType = this.fuelData[0].pour;
    // this.getData();
  }

  // getData() {
  //   this.eventType = this.navParams.get('event');
  //   const paramData = this.navParams.get('paramMaps');
  //   const _url = "https://www.oneqlik.in/gps/getFuelNotifs";
  //   const payload = {
  //     "start_time": paramData.start_time,
  //     "end_time": paramData.end_time,
  //     "imei": paramData.imei,
  //     "event": this.eventType
  //   }
  //   this.apiservice.startLoading().present();
  //   this.apiservice.urlpasseswithdata(_url, payload)
  //     .subscribe(resp => {
  //       this.apiservice.stopLoading();
  //       console.log("response: ", resp)
  //       this.fuelData = resp;
  //     },
  //     err=> {
  //       this.apiservice.stopLoading();
  //     })
  // }

  getAddress(fuelData, index) {
    debugger
    let that = this;
    that.fuelData[index].address = "N/A";
    if (!fuelData.lat) {
      that.fuelData[index].address = "N/A";
    } else if (fuelData.lat) {
      this.geocoderApi.reverseGeocode(Number(fuelData.lat), Number(fuelData.long))
        .then((res) => {
          var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          that.fuelData[index].address = str;
        })
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
