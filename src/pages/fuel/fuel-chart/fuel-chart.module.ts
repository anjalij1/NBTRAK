import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FuelChartPage } from './fuel-chart';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    FuelChartPage,
  ],
  imports: [
    IonicPageModule.forChild(FuelChartPage),
    SelectSearchableModule
  ],
})
export class FuelChartPageModule {}
