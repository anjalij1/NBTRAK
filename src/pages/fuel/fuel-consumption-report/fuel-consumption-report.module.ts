import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FuelConsumptionReportPage } from './fuel-consumption-report';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { OnCreate } from './dummy-directive';

@NgModule({
  declarations: [
    FuelConsumptionReportPage,
    OnCreate
  ],
  imports: [
    IonicPageModule.forChild(FuelConsumptionReportPage),
    SelectSearchableModule,
  ],
  exports: [
    OnCreate
  ],
})
export class FuelConsumptionReportPageModule {}
