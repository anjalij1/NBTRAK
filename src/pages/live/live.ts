import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, AlertController, ModalController, Platform, ViewController, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { SocialSharing } from '@ionic-native/social-sharing';
import * as io from 'socket.io-client';
import * as _ from "lodash";
import { GoogleMaps, Marker, LatLng, Spherical, GoogleMapsEvent, GoogleMapsMapTypeId, LatLngBounds, ILatLng, Polygon, GoogleMapsAnimation } from '@ionic-native/google-maps';
import { Subscription } from 'rxjs/Subscription';
import { PoiPage } from '../live-single-device/live-single-device';
import { DrawerState } from 'ion-bottom-drawer';

@IonicPage()
@Component({
  selector: 'page-live',
  templateUrl: 'live.html',
})
export class LivePage implements OnInit, OnDestroy {

  shouldBounce = true;
  dockedHeight = 52;
  distanceTop = 150;
  drawerState = DrawerState.Docked;
  states = DrawerState;
  minimumHeight = 52;

  ongoingGoToPoint: any = {};
  ongoingMoveMarker: any = {};

  data: any = {};
  _io: any;
  socketSwitch: any = {};
  socketChnl: any = [];
  socketData: any = {};
  allData: any = {};
  userdetails: any;
  showBtn: boolean;
  SelectVehicle: string;
  selectedVehicle: any;
  titleText: any;
  portstemp: any;
  gpsTracking: any;
  power: any;
  currentFuel: any;
  last_ACC: any;
  today_running: string;
  today_stopped: string;
  timeAtLastStop: string;
  distFromLastStop: any;
  lastStoppedAt: string;
  fuel: any;
  total_odo: any;
  todays_odo: any;
  vehicle_speed: any;
  liveVehicleName: any;
  onClickShow: boolean;
  address: any;
  resToken: string;
  liveDataShare: any;
  isEnabled: boolean = false;
  showMenuBtn: boolean = false;
  latlngCenter: any;
  mapHideTraffic: boolean = false;
  mapData: any = [];
  last_ping_on: any;
  geodata: any = [];
  geoShape: any = [];
  generalPolygon: any;
  locations: any = [];
  acModel: any;
  locationEndAddress: any;
  tempaddress: any;
  mapKey: string;
  shwBckBtn: boolean = false;
  recenterMeLat: any;
  recenterMeLng: any;
  menuActive: boolean;
  deviceDeatils: any = {};
  impkey: any;
  showaddpoibtn: boolean = false;
  isSuperAdmin: boolean;
  isDealer: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public actionSheetCtrl: ActionSheetController,
    public elementRef: ElementRef,
    private socialSharing: SocialSharing,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public plt: Platform,
    private viewCtrl: ViewController,
    private toastCtrl: ToastController
  ) {
    var selectedMapKey;
    if (localStorage.getItem('MAP_KEY') != null) {
      selectedMapKey = localStorage.getItem('MAP_KEY');
      if (selectedMapKey == 'Hybrid') {
        this.mapKey = 'MAP_TYPE_HYBRID';
      } else if (selectedMapKey == 'Normal') {
        this.mapKey = 'MAP_TYPE_NORMAL';
      } else if (selectedMapKey == 'Terrain') {
        this.mapKey = 'MAP_TYPE_TERRAIN';
      } else if (selectedMapKey == 'Satellite') {
        this.mapKey = 'MAP_TYPE_HYBRID';
      }
    } else {
      this.mapKey = 'MAP_TYPE_NORMAL';
    }
    this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> " + JSON.stringify(this.userdetails));
    this.menuActive = false;
  }

  car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';

  carIcon = {
    path: this.car,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: 'blue',
    anchor: [12.5, 12.5], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
  };

  icons = {
    "car": this.carIcon,
    "bike": this.carIcon,
    "truck": this.carIcon,
    "bus": this.carIcon,
    "user": this.carIcon,
    "jcb": this.carIcon,
    "tractor": this.carIcon
  }
  goBack() {
    this.navCtrl.pop({ animate: true, direction: 'forward' });
  }

  onClickMainMenu(item) {
    this.menuActive = !this.menuActive;
  }

  refreshMe() {
    this.ngOnDestroy();
    this.ngOnInit();
  }

  resumeListener: Subscription = new Subscription();
  ionViewWillEnter() {
    if (this.plt.is('ios')) {
      this.shwBckBtn = true;
      this.viewCtrl.showBackButton(false);
    }

    this.plt.ready().then(() => {
      this.resumeListener = this.plt.resume.subscribe(() => {
        var today, Christmas;
        today = new Date();
        Christmas = new Date(JSON.parse(localStorage.getItem("backgroundModeTime")));
        var diffMs = (today - Christmas); // milliseconds between now & Christmas
        var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
        if (diffMins >= 5) {
          localStorage.removeItem("backgroundModeTime");
          this.alertCtrl.create({
            message: "Its been 5 mins or more since the app was in background mode. Do you want to reload the screen?",
            buttons: [
              {
                text: 'YES PROCEED',
                handler: () => {
                  // this.getdevices();
                }
              },
              {
                text: 'Back',
                handler: () => {
                  this.navCtrl.setRoot('DashboardPage');
                }
              }
            ]
          }).present();
        }
      })
    })
  }

  ionViewWillLeave() {
    this.plt.ready().then(() => {
      this.resumeListener.unsubscribe();
    })
  }
  newMap() {
    let mapOptions = {
      controls: {
        zoom: false
      },
      mapTypeControlOptions: {
        mapTypeIds: [GoogleMapsMapTypeId.ROADMAP, 'map_style']
      },
      gestures: {
        rotate: false,
        tilt: false
      },
      mapType: this.mapKey
    }
    let map = GoogleMaps.create('map_canvas', mapOptions);
    if (this.plt.is('android')) {
      map.animateCamera({
        target: { lat: 20.5937, lng: 78.9629 },
        duration: 2000,
        // padding: 10,  // default = 20px
      })
      map.setPadding(20, 20, 20, 20);
    }
    return map;
  }

  onClickMap(maptype) {
    let that = this;
    if (maptype == 'SATELLITE') {
      that.allData.map.setMapTypeId(GoogleMapsMapTypeId.SATELLITE);
    } else {
      if (maptype == 'TERRAIN') {
        that.allData.map.setMapTypeId(GoogleMapsMapTypeId.TERRAIN);
      } else {
        if (maptype == 'NORMAL') {
          that.allData.map.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
        }
      }
    }
  }

  settings() {
    let that = this;
    let profileModal = this.modalCtrl.create('DeviceSettingsPage', {
      param: that.deviceDeatils
    });
    profileModal.present();

    profileModal.onDidDismiss(() => {
      for (var i = 0; i < that.socketChnl.length; i++)
        that._io.removeAllListeners(that.socketChnl[i]);

      var paramData = this.navParams.get("device");
      this.temp(paramData);
    })
  }

  addPOI() {
    let that = this;
    let modal = this.modalCtrl.create(PoiPage, {
      param1: that.allData[that.impkey]
    });
    modal.onDidDismiss((data) => {
      console.log(data)
      let that = this;
      that.showaddpoibtn = false;
    });
    modal.present();
  }

  onSelectMapOption(type) {
    let that = this;
    if (type == 'locateme') {
      that.allData.map.setCameraTarget(that.latlngCenter);
    } else {
      if (type == 'mapHideTraffic') {
        that.mapHideTraffic = !that.mapHideTraffic;
        if (that.mapHideTraffic) {
          that.allData.map.setTrafficEnabled(true);
        } else {
          that.allData.map.setTrafficEnabled(false);
        }
      } else {
        if (type == 'showGeofence') {
          console.log("Show Geofence")
          if (that.generalPolygon != undefined) {
            that.generalPolygon.remove();
            that.generalPolygon = undefined;
          } else {
            that.callGeofence();
          }
        }
      }
    }
  }

  callGeofence() {
    this.geoShape = [];
    this.apiCall.startLoading().present();
    this.apiCall.getGeofenceCall(this.userdetails._id)
      .subscribe(data => {
        this.apiCall.stopLoading();
        console.log("geofence data=> " + data.length)
        if (data.length > 0) {
          this.geoShape = data.map(function (d) {
            return d.geofence.coordinates[0];
          });
          for (var g = 0; g < this.geoShape.length; g++) {
            for (var v = 0; v < this.geoShape[g].length; v++) {
              this.geoShape[g][v] = this.geoShape[g][v].reverse()
            }
          }

          for (var t = 0; t < this.geoShape.length; t++) {
            this.drawPolygon(this.geoShape[t])
          }
        } else {
          let alert = this.alertCtrl.create({
            message: 'No gofence found..!!',
            buttons: ['OK']
          });
          alert.present();
        }
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err)
        });
  }

  drawPolygon(polyData) {
    let that = this;
    that.geodata = [];
    that.geodata = polyData.map(function (d) {
      return { lat: d[0], lng: d[1] }
    });
    console.log("geodata=> ", that.geodata)

    // let bounds = new LatLngBounds(that.geodata);
    // that.allData.map.moveCamera({
    // target: bounds
    // });
    let GORYOKAKU_POINTS: ILatLng[] = that.geodata;
    console.log("GORYOKAKU_POINTS=> ", GORYOKAKU_POINTS)
    that.allData.map.addPolygon({
      'points': GORYOKAKU_POINTS,
      'strokeColor': '#AA00FF',
      'fillColor': '#00FFAA',
      'strokeWidth': 2
    }).then((polygon: Polygon) => {
      // polygon.on(GoogleMapsEvent.POLYGON_CLICK).subscribe((param) => {
      //   console.log("polygon data=> " + param)
      //   console.log("polygon data=> " + param[1])
      // })
      this.generalPolygon = polygon;
    });
  }

  ngOnInit() {

    if (localStorage.getItem("SCREEN") != null) {
      if (localStorage.getItem("SCREEN") === 'live') {
        this.showMenuBtn = true;
      }
    }

    this._io = io.connect(this.apiCall.mainUrl + 'gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
    this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });

    // this.drawerHidden = true;
    this.drawerState = DrawerState.Bottom;
    this.onClickShow = false;

    this.showBtn = false;
    this.SelectVehicle = "Select Vehicle";
    this.selectedVehicle = undefined;
    this.userDevices();
  }

  ngOnDestroy() {
    for (var i = 0; i < this.socketChnl.length; i++)
      this._io.removeAllListeners(this.socketChnl[i]);
    this._io.on('disconnect', () => {
      this._io.open();
    })
  }

  shareLive() {
    var data = {
      id: this.liveDataShare._id,
      imei: this.liveDataShare.Device_ID,
      sh: this.userdetails._id,
      ttl: 60 // set to 1 hour by default
    };
    this.apiCall.startLoading().present();
    this.apiCall.shareLivetrackCall(data)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.resToken = data.t;
        this.liveShare();
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  getPOIs() {
    this.allData._poiData = [];
    const _burl = this.apiCall.mainUrl + "poi/getPois?user=" + this.userdetails._id;
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(_burl)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.allData._poiData = data;

        this.plotPOI();
      });
  }

  plotPOI() {
    let toast = this.toastCtrl.create({
      message: "Plotting POI's please wait...",
      position: 'middle'
    });
    for (var v = 0; v < this.allData._poiData.length; v++) {
      toast.present();
      if (this.allData._poiData[v].poi.location.coordinates !== undefined) {
        this.allData.map.addMarker({
          title: this.allData._poiData[v].poi.poiname,
          position: {
            lat: this.allData._poiData[v].poi.location.coordinates[1],
            lng: this.allData._poiData[v].poi.location.coordinates[0]
          },
          icon: './assets/imgs/poiFlag.png',
          animation: GoogleMapsAnimation.BOUNCE
        })
      }
    }
    toast.dismiss();
  }

  liveShare() {
    let that = this;
    var link = this.apiCall.mainUrl + "share/liveShare?t=" + that.resToken;
    that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
  }

  zoomin() {
    let that = this;
    that.allData.map.moveCameraZoomIn();
  }
  zoomout() {
    let that = this;
    that.allData.map.animateCameraZoomOut();
  }

  userDevices() {
    var baseURLp;
    let that = this;
    if (that.allData.map != undefined) {
      that.allData.map.remove();
      that.allData.map = that.newMap();
    } else {
      that.allData.map = that.newMap();
    }

    baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.userdetails._id + '&email=' + this.userdetails.email;

    if (this.userdetails.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.userdetails._id;
      this.isSuperAdmin = true;
    } else {
      if (this.userdetails.isDealer == true) {
        baseURLp += '&dealer=' + this.userdetails._id;
        this.isDealer = true;
      }
    }

    that.apiCall.startLoading().present();
    that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(resp => {
        that.apiCall.stopLoading();
        that.portstemp = resp.devices;
        console.log("list of vehicles :", that.portstemp)
        that.mapData = [];
        that.mapData = that.portstemp.map(function (d) {
          if (d.last_location != undefined) {
            return { lat: d.last_location['lat'], lng: d.last_location['long'] };
          }
        });

        let bounds = new LatLngBounds(that.mapData);
        that.allData.map.moveCamera({
          target: bounds,
          zoom: 10
        })

        // this.innerFunc(that.portstemp);

        for (var i = 0; i < resp.devices.length; i++) {
          console.log("status: ", resp.devices[i].status)
          if (resp.devices[i].status != "Expired") {
            if (that.isSuperAdmin || that.isDealer) {
              // that.getIconUrl(resp.devices[i])
              that.allData.map.addMarker({
                title: resp.devices[i].Device_Name,
                position: {
                  lat: resp.devices[i].last_location.lat,
                  lng: resp.devices[i].last_location.long
                },
                icon: that.getIconUrl(resp.devices[i])
              })
            } else {
              that.socketInit(resp.devices[i]);
            }
          }
          // that.socketInit(resp.devices[i]); 
        }
      },
        err => {
          that.apiCall.stopLoading();
          console.log(err);
        });
  }

  getIconUrl(data) {
    let that = this;
    var iconUrl;
    if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) > 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) > 0) {
      if (that.plt.is('ios')) {
        iconUrl = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
      } else if (that.plt.is('android')) {
        iconUrl = './assets/imgs/vehicles/running' + data.iconType + '.png';
      }
    } else {
      if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) === 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) === 0) {
        if (that.plt.is('ios')) {
          iconUrl = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
        } else if (that.plt.is('android')) {
          iconUrl = './assets/imgs/vehicles/idling' + data.iconType + '.png';
        }
      } else {
        if (that.plt.is('ios')) {
          iconUrl = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
        } else if (that.plt.is('android')) {
          iconUrl = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
        }
      }
    }
    return iconUrl;
  }



  parseMillisecondsIntoReadableTime(milliseconds) {
    //Get hours from milliseconds
    var hours = milliseconds / (1000 * 60 * 60);
    var absoluteHours = Math.floor(hours);
    var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;

    //Get remainder from hours and convert to minutes
    var minutes = (hours - absoluteHours) * 60;
    var absoluteMinutes = Math.floor(minutes);
    var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;

    //Get remainder from minutes and convert to seconds
    var seconds = (minutes - absoluteMinutes) * 60;
    var absoluteSeconds = Math.floor(seconds);
    var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;

    // return h + ':' + m;
    return h + ':' + m + ':' + s;
  }

  socketInit(pdata, center = false) {
    let that = this;

    that._io.emit('acc', pdata.Device_ID);
    that.socketChnl.push(pdata.Device_ID + 'acc');
    that._io.on(pdata.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {

      if (d4 != undefined)
        (function (data) {

          if (data == undefined) {
            return;
          }

          if (data._id != undefined && data.last_location != undefined) {
            var key = data._id;
            that.impkey = data._id;
            that.deviceDeatils = data;
            let ic = _.cloneDeep(that.icons[data.iconType]);
            if (!ic) {
              return;
            }
            ic.path = null;
            if (data.status.toLowerCase() === 'running' && data.last_speed > 0) {
              if (that.plt.is('ios')) {
                ic.url = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
              } else if (that.plt.is('android')) {
                ic.url = './assets/imgs/vehicles/running' + data.iconType + '.png';
              }
            } else {
              if (data.status.toLowerCase() === 'running' && data.last_speed === 0) {
                if (that.plt.is('ios')) {
                  ic.url = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
                } else if (that.plt.is('android')) {
                  ic.url = './assets/imgs/vehicles/idling' + data.iconType + '.png';
                }
              } else {
                if (data.status.toLowerCase() === 'idling' && data.last_speed === 0) {
                  if (that.plt.is('ios')) {
                    ic.url = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
                  } else if (that.plt.is('android')) {
                    ic.url = './assets/imgs/vehicles/idling' + data.iconType + '.png';
                  }
                } else {
                  if (data.status.toLowerCase() === 'idling' && data.last_speed > 0) {
                    if (that.plt.is('ios')) {
                      ic.url = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
                    } else if (that.plt.is('android')) {
                      ic.url = './assets/imgs/vehicles/running' + data.iconType + '.png';
                    }
                  } else {
                    if (that.plt.is('ios')) {
                      ic.url = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                    } else if (that.plt.is('android')) {
                      ic.url = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                    }
                  }
                }
              }
            }

            console.log("ic url=> " + ic.url)
            that.vehicle_speed = data.last_speed;
            that.todays_odo = data.today_odo;
            that.total_odo = data.total_odo;
            // that.fuel = data.currentFuel;
            if (that.userdetails.fuel_unit == 'LITRE') {
              that.fuel = data.currentFuel;
            } else if (that.userdetails.fuel_unit == 'PERCENTAGE') {
              that.fuel = data.fuel_percent;
            } else {
              that.fuel = data.currentFuel;
            }
            that.last_ping_on = data.last_ping_on;

            if (data.lastStoppedAt != null) {
              var fd = new Date(data.lastStoppedAt).getTime();
              var td = new Date().getTime();
              var time_difference = td - fd;
              var total_min = time_difference / 60000;
              var hours = total_min / 60
              var rhours = Math.floor(hours);
              var minutes = (hours - rhours) * 60;
              var rminutes = Math.round(minutes);
              that.lastStoppedAt = rhours + ':' + rminutes;
            } else {
              that.lastStoppedAt = '00' + ':' + '00';
            }

            that.distFromLastStop = data.distFromLastStop;
            if (!isNaN(data.timeAtLastStop)) {
              that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
            } else {
              that.timeAtLastStop = '00:00:00';
            }
            that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
            that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
            that.last_ACC = data.last_ACC;
            that.acModel = data.ac;
            that.currentFuel = data.currentFuel;
            that.power = data.power;
            that.gpsTracking = data.gpsTracking;
            that.recenterMeLat = data.last_location.lat;
            that.recenterMeLng = data.last_location.long;

            if (that.allData[key]) {
              that.socketSwitch[key] = data;
              if (that.allData[key].mark != undefined) {
                that.allData[key].mark.setIcon(ic);
                that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                var temp = _.cloneDeep(that.allData[key].poly[1]);
                that.allData[key].poly[0] = _.cloneDeep(temp);
                that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };

                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
              } else {
                return;
              }
            }
            else {
              that.allData[key] = {};
              that.socketSwitch[key] = data;
              that.allData[key].poly = [];

              that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });

              if (data.last_location != undefined) {

                that.allData.map.addMarker({
                  title: data.Device_Name,
                  position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                  icon: ic,
                }).then((marker: Marker) => {

                  that.allData[key].mark = marker;

                  // if (that.selectedVehicle == undefined) {
                  //   marker.showInfoWindow();
                  // }

                  // if (that.selectedVehicle != undefined) {
                  marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
                    .subscribe(e => {
                      if (that.selectedVehicle == undefined) {
                        that.getAddressTitle(marker);
                      } else {
                        that.liveVehicleName = data.Device_Name;
                        // that.drawerHidden = false;
                        that.showaddpoibtn = true;
                        that.drawerState = DrawerState.Docked;
                        that.onClickShow = true;
                      }
                    });
                  // }
                  var speed = data.status == "RUNNING" ? data.last_speed : 0;
                  that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                })
              }
            }
            // if (that.selectedVehicle != undefined) {
            //   var geocoder = new google.maps.Geocoder;
            //   var latlng = new google.maps.LatLng(data.last_location['lat'], data.last_location['long']);

            //   var request = {
            //     "latLng": latlng
            //   };

            //   geocoder.geocode(request, function (resp, status) {
            //     if (status == google.maps.GeocoderStatus.OK) {

            //       if (resp[0] != null) {
            //         that.address = resp[0].formatted_address;
            //       } else {
            //         console.log("No address available")
            //       }
            //     }
            //     else {
            //       that.address = 'N/A';
            //     }
            //   })
            // }

          }
        })(d4)
    })
  }

  reCenterMe() {
    // console.log("getzoom level: " + this.allData.map.getCameraZoom());
    this.allData.map.moveCamera({
      target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
      zoom: this.allData.map.getCameraZoom()
    }).then(() => {

    })
  }

  getAddressTitle(marker) {
    var payload = {
      "lat": marker.getPosition().lat,
      "long": marker.getPosition().lng,
      "api_id": "1"
    }
    var addr;
    this.apiCall.getAddressApi(payload)
      .subscribe((data) => {
        console.log("got address: " + data.results)
        if (data.results[2] != undefined || data.results[2] != null) {
          addr = data.results[2].formatted_address;
        } else {
          addr = 'N/A';
        }
        marker.setSnippet(addr);
      })
  }

  addCluster() {

    let that = this;
    console.log("locations length=> " + that.locations.length)
    console.log("locations=> " + that.locations)
    var icicon;
    if (that.plt.is('ios')) {
      icicon = "www/assets/imgs/maps/m1.png";
    } else if (that.plt.is('android')) {
      icicon = "./assets/imgs/maps/m1.png";
    }
    that.allData.map.addMarkerCluster({
      markers: that.locations,
      icons: [
        { min: 50, max: 100, url: icicon, anchor: { x: 16, y: 16 } }
      ]
    })
      .then((markerCluster) => {
        // markerCluster.on(GoogleMapsEvent.CLUSTER_CLICK).subscribe((cluster: any) => {
        //   console.log('cluster was clicked.');
        // });
      });
  }
  liveTrack(map, mark, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {

    var target = 0;

    clearTimeout(that.ongoingGoToPoint[id]);
    clearTimeout(that.ongoingMoveMarker[id]);

    if (center) {
      map.setCameraTarget(coords[0]);
    }

    function _goToPoint() {
      if (target > coords.length)
        return;

      var lat = mark.getPosition().lat;
      var lng = mark.getPosition().lng;

      var step = (speed * 1000 * delay) / 3600000; // in meters
      if (coords[target] == undefined)
        return;

      var dest = new LatLng(coords[target].lat, coords[target].lng);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target].lat - lat) / numStep;
      var deltaLng = (coords[target].lng - lng) / numStep;

      function changeMarker(mark, deg) {
        mark.setRotation(deg);
        mark.setIcon(icons);
      }
      function _moveMarker() {

        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        if (i < distance) {
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          if (that.selectedVehicle != undefined) {
            map.setCameraTarget(new LatLng(lat, lng));
          }
          that.latlngCenter = new LatLng(lat, lng);
          mark.setPosition(new LatLng(lat, lng));
          that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
        }
        else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          if (that.selectedVehicle != undefined) {
            map.setCameraTarget(dest);
          }
          that.latlngCenter = dest;
          mark.setPosition(dest);
          target++;
          that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
        }
      }
      _moveMarker();
    }
    _goToPoint();
  }

  temp(data) {
    // debugger
    if (data.status == 'Expired') {
      let profileModal = this.modalCtrl.create('ExpiredPage');
      profileModal.present();

      profileModal.onDidDismiss(() => {
        this.selectedVehicle = undefined;
      });

    } else {
      let that = this;
      that.liveDataShare = data;
      // that.drawerHidden = true;
      that.drawerState = DrawerState.Bottom;
      that.onClickShow = false;

      if (that.allData.map != undefined) {
        // that.allData.map.clear();
        that.allData.map.remove();
      }

      console.log("on select change data=> " + JSON.stringify(data));
      for (var i = 0; i < that.socketChnl.length; i++)
        that._io.removeAllListeners(that.socketChnl[i]);
      that.allData = {};
      that.socketChnl = [];
      that.socketSwitch = {};
      if (data) {
        if (data.last_location) {

          let mapOptions = {
            // backgroundColor: 'white',
            controls: {
              compass: true,
              zoom: false,
              mapToolbar: true
            },
            gestures: {
              rotate: false,
              tilt: false
            },
            mapType: that.mapKey
          }
          let map = GoogleMaps.create('map_canvas', mapOptions);
          map.animateCamera({
            target: { lat: 20.5937, lng: 78.9629 },
            zoom: 15,
            duration: 1000,
            padding: 0  // default = 20px
          });

          map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
          map.setPadding(20, 20, 20, 20);
          // map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
          that.allData.map = map;
          that.socketInit(data);
        } else {
          that.allData.map = that.newMap();
          that.socketInit(data);
        }

        if (that.selectedVehicle != undefined) {
          // that.drawerHidden = false;
          that.drawerState = DrawerState.Docked;
          that.onClickShow = true;
        }
      }
      that.showBtn = true;
    }
  }
  info(mark, cb) {
    mark.addListener('click', cb);
  }

  setDocHeight() {
    let that = this;
    that.drawerState = DrawerState.Top;
  }
}
