import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';


@IonicPage()
@Component({
  selector: 'page-fastag',
  templateUrl: 'fastag.html',
})
export class FastagPage {
  buttonColor: string = '#fff'; //Default Color
  buttonColor1: string = '#fff';
  buttonColor2: string;
  buttonColor3: string;
  buttonColor4: string;
  buttonColor5: string;
  islogin: any;
  vehicleType: string;
  numFastag: number;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private apiCall: ApiServiceProvider,
    private toastCtrl: ToastController) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FastagPage');
  }
  addEvent() {
    this.vehicleType = 'Truck';
    this.buttonColor1 = '#fff';
    this.buttonColor2 = '#fff';
    this.buttonColor3 = '#fff';
    this.buttonColor4 = '#fff';
    this.buttonColor5 = '#fff';
    this.buttonColor = '#93d2f2'; //desired Color
  }

  addEvent1() {
    this.vehicleType = 'Bus';
    this.buttonColor = '#fff';
    this.buttonColor2 = '#fff';
    this.buttonColor3 = '#fff';
    this.buttonColor4 = '#fff';
    this.buttonColor5 = '#fff';
    this.buttonColor1 = '#93d2f2'; //desired Color
  }

  addEvent2() {
    this.vehicleType = 'Taxi';
    this.buttonColor1 = '#fff';
    this.buttonColor = '#fff';
    this.buttonColor3 = '#fff';
    this.buttonColor4 = '#fff';
    this.buttonColor5 = '#fff';
    this.buttonColor2 = '#93d2f2'; //desired Color
  }

  addEvent3() {
    this.vehicleType = 'Car';
    this.buttonColor2 = '#fff';
    this.buttonColor1 = '#fff';
    this.buttonColor = '#fff';
    this.buttonColor4 = '#fff';
    this.buttonColor5 = '#fff';
    this.buttonColor3 = '#93d2f2'; //desired Color
  }
  addEvent4() {
    this.vehicleType = 'Bike';
    this.buttonColor2 = '#fff';
    this.buttonColor1 = '#fff';
    this.buttonColor = '#fff';
    this.buttonColor3 = '#fff';
    this.buttonColor5 = '#fff';
    this.buttonColor4 = '#93d2f2'; //desired Color
  }

  addEvent5() {
    this.vehicleType = 'Other';
    this.buttonColor4 = '#fff';
    this.buttonColor2 = '#fff';
    this.buttonColor1 = '#fff';
    this.buttonColor = '#fff';
    this.buttonColor3 = '#fff';
    this.buttonColor5 = '#93d2f2'; //desired Color
  }

  fastagReq() {
    if (this.vehicleType === undefined || this.numFastag === undefined) {
      this.toastCtrl.create({
        message: 'Please select the vehicle type and add number of requests.',
        duration: 2000,
        position: 'bottom'
      }).present();
      return;
    }
    var url = this.apiCall.mainUrl + "fastTag/addRequest";
    var payload = {};
    // debugger
    if (this.islogin.Dealer_ID === undefined) {
      payload = {
        user: this.islogin._id,
        Dealer: this.islogin.supAdmin,
        supAdmin: this.islogin.supAdmin,
        date: new Date().toISOString(),
        quantity: this.numFastag,
        vehicle_type: this.vehicleType
      }
    } else {
      payload = {
        user: this.islogin._id,
        Dealer: this.islogin.Dealer_ID._id,
        supAdmin: this.islogin.supAdmin,
        date: new Date().toISOString(),
        quantity: this.numFastag,
        vehicle_type: this.vehicleType
      }
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(url, payload)
      .subscribe(respData => {
        this.apiCall.stopLoading();
        console.log('response data: ', respData);
        if (respData.message === 'saved succesfully') {
          this.toastCtrl.create({
            message: 'Fastag added successfully.',
            duration: 2000,
            position: 'bottom'
          }).present();
          this.navCtrl.pop();
        }
      },
        err => {
          this.apiCall.stopLoading();
        });
  }
}
